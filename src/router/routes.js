import Flights from '@/components/Flights'
import FlightsList from '@/components/FlightsList'
import FlightDetails from '@/components/FlightDetails'

export const routes = [{
  path: '*', redirect: '/'
},
{
  path: '/',
  component: Flights,
  children: [{
    path: '/',
    redirect: {
      name: 'FlightsList'
    }
  },
  {
    name: 'FlightsList',
    path: 'flights',
    component: FlightsList
  },
  {
    name: 'FlightDetails',
    path: 'flightDetails/:id',
    component: FlightDetails
  }]
}]

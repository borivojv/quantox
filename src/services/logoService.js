import axios from 'axios'

export default {
  api () {
    return axios.create({
      baseURL: `https://autocomplete.clearbit.com/v1/companies`
    })
  },
  fetchLogo (companyName) {
    let url = '/suggest?query=:' + companyName
    return this.api().get(url)
  }
}

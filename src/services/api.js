import axios from 'axios'

export default() => {
  return axios.create({
    baseURL: `https://public-api.adsbexchange.com/VirtualRadar`
  })
}

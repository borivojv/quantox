import axios from 'axios'

export default {
  api () {
    return axios.create({
      baseURL: `https://public-api.adsbexchange.com/VirtualRadar`
    })
  },
  fetchFlights (params) {
    let url = 'AircraftList.json?'

    for (let param in params) {
      url += param + '=' + params[param] + '&'
    }

    url = url.charAt(url.length - 1) === '&' ? url.slice(0, -1) : url
    return this.api().get(url)
  }
}

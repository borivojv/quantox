// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueGeolocation from 'vue-browser-geolocation'
import moment from 'vue-moment'
import BootstrapVue from 'bootstrap-vue'

Vue.config.productionTip = false

Vue.use(VueGeolocation)
Vue.use(BootstrapVue)
Vue.use(moment)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
